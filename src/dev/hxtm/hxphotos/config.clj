(ns dev.hxtm.hxphotos.config
  (:require [clojure.string :as str]))

(defn config
  "Returns the configuration option set using an uppercase environment variable
   or the fallback value if not found.
   If the fallback is a number the value is parsed as a double."
  [k fallback]
  (or (some-> (System/getenv (str/upper-case (name k)))
              ((if (number? fallback) parse-double identity)))
      fallback))