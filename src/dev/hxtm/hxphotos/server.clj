(ns dev.hxtm.hxphotos.server
  (:gen-class)
  (:require [cheshire.core :as json]
            [clojure.core.async :as a :refer [<! <!! >! >!! chan close! go
                                              thread timeout]]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.set :as set]
            [clojure.string :as str]
            [dev.hxtm.hxphotos.config :refer [config]]
            [dev.hxtm.hxphotos.html :as html]
            [reitit.ring :as ring]
            [reitit.ring.middleware.parameters :as parameters]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.util.codec :as codec])
  (:import [java.awt.image RenderedImage]
           [java.io ByteArrayOutputStream File FileNotFoundException]
           [java.security DigestInputStream MessageDigest]
           [javax.imageio
            IIOImage
            ImageIO
            ImageWriteParam
            ImageWriter]
           [net.coobird.thumbnailator Thumbnails Thumbnails$Builder]
           [net.coobird.thumbnailator.geometry Positions]))

(defn log
  "Logs the arguments separated by a space.
   Safe to use in multiple threads at once, everything from one call will be logged atomically."
  [& args]
  (print (str (str/join " " (map str args)) ;; to avoid mixing lines from different threads 
              \newline))
  (flush))

(set! *warn-on-reflection* true)

(def db-file (io/file (config :cachefile "hxphotos.cache")))
(def current-version (config :version ""))

(defn calculate-digest
  "Calculates a short and URL-friendly digest for the contents of a File."
  [file]
  (with-open [dis (DigestInputStream. (io/input-stream file)
                                      (MessageDigest/getInstance "SHA-256"))]
    (while (not-empty (.readNBytes dis 8192)))
    (-> (->> (.. dis getMessageDigest digest)
             (take 9)
             (byte-array)
             codec/base64-encode)
        (str/replace "/" "-")
        (str/replace "+" "_"))))

(defn serve-immutable
  "Creates a response with cache control headers indicating it can be cached forever.
   The function is applied to the :digest path parameter and the result is used as the :body.
   Returns a 404 response if the result is logical false."
  [f]
  #(if-let [file (-> % :path-params :digest f)]
     {:body file
      :headers {"cache-control" "max-age=31536000, immutable"}}
     {:status 404}))

(defn data-directory [] (config :data "."))

(defn add-new-files
  "Returns a sequence of states observed while looking for new photo files."
  [{:keys [photos]}]
  (log "Adding new files")
  (let [known (->> photos vals (map :files) (apply set/union))]
    (->> (file-seq (io/file (data-directory)))
         (map str)
         (filter #(str/ends-with? % ".jpg"))
         (remove known)
         (map #(let [d (calculate-digest %)]
                 {d {:files #{%} :digest d}}))
         (reductions (fn [existing new]
                       (merge-with #(-> %1
                                        (update :files set/union (:files %2))
                                        (dissoc :version))
                                   existing
                                   new))
                     photos)
         (map #(hash-map :msg "SCANNING"
                         :photos %)))))

(defn within-directory?
  "Determines whether a file is within a directory of any of it's recursive subdirectories."
  [file-path dir-path]
  (let [mk-file #(.getCanonicalFile (io/as-file %))]
    (loop [f (mk-file file-path)
           d (mk-file dir-path)]
      (if-some [parent (.getParentFile ^File f)]
        (if (= parent d)
          true
          (recur parent d))
        false))))

(defn remove-missing-files
  "Returns a sequence of states observed while removing photo files
   that no longer exist or are outside of the currently configured data path."
  [{:keys [photos] :as state}]
  (log "Removing missing files")
  (let [visible-and-exists #(and (within-directory? % (data-directory))
                                 (.exists (io/as-file %)))
        remove-missing (fn [paths] (set (filter visible-and-exists paths)))
        update-photo (fn [{:keys [files] :as photo}]
                       (let [remaining (remove-missing files)]
                         (if (= remaining files)
                           photo
                           (-> photo
                               (assoc :files remaining)
                               (dissoc :version)))))]
    (->> photos
         (map #(update % 1 update-photo))
         (filter #(-> (% 1) :files seq))
         (into {})
         (assoc state :photos)
         vector)))

(defn load-db
  "Attempts to load a previously saved database."
  []
  (log "Loading DB")
  (try (-> db-file slurp edn/read-string)
       (catch FileNotFoundException _
         (log "DB file not found, using empty DB")
         {})))

(defn save-db
  "Saves the database to disk."
  [db]
  (log "Saving DB")
  (io/make-parents db-file)
  (spit db-file (prn-str db))
  (log "DB saved")
  db)

(def state
  "Current application state."
  (atom {:photos {}
         :msg "LOADING"}))

(defn init-state
  "Initialized the global state atom."
  []
  (log "Initializing DB")
  (reset! state {:photos (load-db)
                 :msg "LOADING"}))

(defn find-file
  "Finds one of the files known to belong to a digest.
   Returns nil if none are found."
  [digest]
  (some->> ((:photos @state) digest) :files (map io/file) (filter #(.exists ^File %)) first))

(defn write-progressive
  "Returns a byte array of a progressive JPEG for a given RenderedImage.
   The JPEG settings prioritize being useful over bad connections instead of image quality."
  [^RenderedImage image]
  (let [writer ^ImageWriter (.next (ImageIO/getImageWritersByFormatName "jpeg"))
        param (doto (.getDefaultWriteParam writer)
                (.setCompressionMode ImageWriteParam/MODE_EXPLICIT)
                (.setCompressionQuality 0.6)
                (.setProgressiveMode ImageWriteParam/MODE_DEFAULT))]
    (try (with-open [baos (ByteArrayOutputStream.)
                     ios (ImageIO/createImageOutputStream baos)]
           (doto writer
             (.setOutput ios)
             (.write nil (IIOImage. image nil nil) param))
           (.toByteArray baos))
         (finally (.dispose writer)))))

(defn scaled-image
  "Returns a byte array containing a version of the photo identified by digest,
   scaled down to maxres in both dimensions if necessary,
   cropping to the center square if desired."
  [digest maxres square]
  (when-let [original (find-file digest)]
    (-> (Thumbnails/of ^"[Ljava.io.File;" (into-array File [original]))
        (.size maxres maxres)
        ((if square #(.crop ^Thumbnails$Builder % Positions/CENTER) identity))
        (#(.asBufferedImage ^Thumbnails$Builder %))
        write-progressive)))

(defn thumbnail
  "Generates a photo thumbnail for use in lists."
  [digest]
  (scaled-image digest 512 true))

(defn preview
  "Generates a photo preview for fullscreen use
   while saving lots of bandwidth compared to the original."
  [digest]
  (scaled-image digest 2048 false))

(defn distance
  "Calculates the squared Euclidian distance using as many dimensions as the shorter number vector has."
  [a b]
  (->> (map (comp #(* % %) -) a b)
       (apply +)))

(defn query
  "Filters and sorts all photos based on the given request."
  [{:keys [start sort-label filters]
    :or {sort-label :digest}}
   photos]
  (let [make-filter (fn [[k v]]
                      (cond
                        (nil? v) #(nil? (get % k))
                        (true? v) #(some? (get % k))
                        (map? v) (let [{vmin :min vmax :max} v]
                                   #(and (or (nil? vmin) (not (neg? (compare (get % k) vmin))))
                                         (or (nil? vmax) (not (pos? (compare (get % k) vmax))))))))
        start-photo (get photos start)
        reference-point (get start-photo sort-label)
        filters (->> (merge filters {"hide" nil})
                     (map make-filter)
                     (apply every-pred)
                     (some-fn #{start-photo}))]
    (->> (vals photos)
         (filter filters)
         (sort-by (juxt #(let [v (get % sort-label)]
                           (if (vector? v)
                             (distance v reference-point)
                             v))
                        #(not= start-photo %)
                        :digest)))))

(defn query-handler
  "Queries the photo collection and returns a HTML5 page."
  [{:keys [query-string]}]
  (let [{:keys [start] :as request} (when-not (str/blank? query-string)
                                      (-> (str "{" query-string "}")
                                          (codec/percent-decode)
                                          edn/read-string))
        {:keys [photos msg]} @state
        results (query request photos)
        next-photos (fn [backwards]
                      (->> results
                           ((if backwards reverse identity))
                           (drop-while #(and start (not= (:digest %) start)))
                           (drop (if start 1 0))
                           (take (config :load-distance 256))))]
    (html/page {:state msg
                :focused (photos start)
                :before (next-photos true)
                :after (when (:start request)
                         (next-photos false))
                :request request})))

(defn auth
  "Middleware ensuring the configured Basic authentication are provided with the request if configured."
  [handler]
  (if-let [setting (config :auth false)]
    (let [salt (random-uuid)
          salted-digest #(.. (MessageDigest/getInstance "SHA-256")
                             (digest (.getBytes (str salt %))))
          expected-auth (->> ^String setting
                             .getBytes
                             codec/base64-encode
                             (str "Basic ")
                             salted-digest)]
      (fn [request]
        (if (-> request
                :headers
                (get "authorization")
                salted-digest
                (MessageDigest/isEqual expected-auth))
          (handler request)
          {:status 401
           :headers {"WWW-Authenticate" "Basic"}
           :body "Login failed"})))
    handler))

(def handler
  (ring/ring-handler
   (ring/router
    [["/" {:get query-handler}]
     ["/:digest" {:get (serve-immutable find-file)}]
     ["/:digest/preview" {:get (serve-immutable preview)}]
     ["/:digest/thumbnail" {:get (serve-immutable thumbnail)}]]
    {:data {:middleware [parameters/parameters-middleware auth]}})
   (ring/create-default-handler)))

(defn streaming-process
  "Returns a channel of script results from a given script command for the given photo sequence.
   The script is called with the files for the first photo as arguments and a list of file names per photo encoded as a line of JSON on stdin.
   The result can either be sequence of key=value pairs for the first photo separated by newlines
   which are trimmed and only used if non-empty,
   or one JSON object per line with values being either true, a string, a number, or a list of numbers.
   Stream is closed on error, caller must handle this case."
  [cmd photos]
  (let [output (chan)]
    (thread
      (loop [file-seqs (->> photos (map (comp sort :files)))]
        (when (seq file-seqs)
          (let [cmd-args (doto (conj (seq (first file-seqs))
                                     cmd)
                           (#(log "Running" %)))
                process (.exec (Runtime/getRuntime)
                               ^"[Ljava.lang.String;" (into-array cmd-args))]
            (thread (try
                      (with-open [stdin (io/writer (.getOutputStream process))]
                        (run! #(.write stdin (str (json/encode %) \newline))
                              file-seqs))
                      (catch java.io.IOException _))) ; process exited
            (when-let [kv-lines
                       (with-open [stdout (io/reader (.getInputStream process))]
                         (let [lines (line-seq stdout)]
                           (if-not (and (seq lines)
                                        (str/starts-with? (first lines) "{"))
                             (vec lines)
                             (try (doseq [line lines] ; TODO make more functional
                                    (let [decoded (json/decode line)
                                          valid-value (some-fn true?
                                                               string?
                                                               number?
                                                               (every-pred vector
                                                                           (partial every? number?)))]
                                      (if (and (map? decoded)
                                               (every? valid-value (vals decoded)))
                                        (>!! output decoded)
                                        (throw (IllegalArgumentException. "invalid script results")))))
                                  ; stop processing on JSON errors including partial line
                                  (catch com.fasterxml.jackson.core.JsonParseException e
                                    (log cmd "failed" e))
                                  (catch IllegalArgumentException e
                                    (log cmd "failed" e))))))]
              (when (zero? (.waitFor process)) ; don't use partially written =-separated results
                (>!! output (->> kv-lines
                                 (remove str/blank?)
                                 (map #(->> (str/split % #"=" 2)
                                            (mapv str/trim)))
                                 (remove #(-> % count (< 2)))
                                 (remove #(some str/blank? %))
                                 (into {})))
                (recur (rest file-seqs)))))))
      (close! output))
    output))

(defn as-seq
  "Returns a lazy sequence with all items from a channel."
  [ch]
  (lazy-seq
   (when-some [v (<!! ch)]
     (cons v (as-seq ch)))))

(defn retry
  "Retries a function that returns a channel of results given a sequence of inputs
   until results for all inputs have been obtained.
   If a function invocation did not yield a single result it is first retried immediately,
   then after 5 seconds, then 7.5 etc. multiplying the interval by 1.5 each time.
   A successful result resets the backoff timer."
  [f input]
  (let [output (chan)]
    (go
      (loop [to-process input
             process (f to-process)
             next-wait 0]
        (when (seq to-process)
          (if-some [v (<! process)]
            (do (>! output v)
                (recur (rest to-process) process 0))
            (do (when (pos? next-wait)
                  (log "Retrying script after" next-wait "ms")
                  (<! (timeout next-wait)))
                (recur to-process
                       (f to-process)
                       (-> next-wait (* 1.5) long (max 5000)))))))
      (close! output))
    output))

(defn update-labels
  "Updates the labels of photos that aren't marked with the currently configured VERSION."
  [{:keys [photos]}]
  (let [to-update (->> photos vals (remove (comp #{current-version} :version)))
        total (count to-update)
        scripts (->> (io/file (config :scripts "hxphotos_scripts"))
                     .listFiles
                     (filter #(and (.isFile ^File %)
                                   (.canExecute ^File %)))
                     (map str)
                     sort)
        update-photo (fn [photo labels] (merge
                                         (select-keys photo [:digest :files])
                                         {:version current-version}
                                         labels))
        new-labels (->> scripts
                        (map #(retry (partial streaming-process %)
                                     to-update))
                        (a/map merge)
                        as-seq)
        with-index (map-indexed vector to-update)]
    (reductions (fn [{:keys [photos]} [idx {:keys [digest]} labels]]
                  {:msg (-> idx inc (/ total) (* 100) int (str "% ANALYZING"))
                   :photos (update photos
                                   digest
                                   update-photo
                                   labels)})
                {:msg "0% ANALYZING"
                 :photos photos}
                (map conj with-index new-labels))))

(defn rescan-files
  "Updates the state atom with all the intermediate states resulting from
   updating the list of found files and (re)analyzing photos with changes using the scripts."
  []
  (log "Rescanning files")
  (let [applying-reductions
        (partial comp last
                 (partial map (partial reset! state)))
        process (->> [add-new-files
                      remove-missing-files
                      update-labels]
                     (map applying-reductions)
                     reverse (apply comp))]
    (->> @state
         process
         :photos
         save-db
         (hash-map :photos)
         (reset! state))))

(defn -main []
  (init-state)
  (thread (rescan-files))
  (let [host (config :host "127.0.0.1")
        port (config :port 8080)]
    (log "Starting server on" (str host \: port))
    (run-jetty #'handler {:host host
                          :port port
                          :join? false
                          :max-threads (config :threads 8)})))

(comment
  ;; for development
  (-main)

  ;; use this in interactive development to trigger a rescan
  (do (def current-version (str (hash current-version)))
      (rescan-files)))