(ns dev.hxtm.hxphotos.html
  (:require [clojure.string :as str]
            [hiccup2.core :refer [html]]
            [ring.util.codec :as codec])
  (:import [java.text DecimalFormat]))

(defn style
  "Creates a CSS style string from a map of style properties."
  [& {:as styles}]
  (->> styles
       (map (fn [[k v]] (str (name k) ": " v)))
       (str/join "; ")))

(defn remove-empty-maps
  "Recursively removes map entries with empty map values.
   Useful for simplifying query strings."
  [m]
  (->> m
       (map (fn [[k v]]
              (if (map? v)
                (some->> (remove-empty-maps v)
                         (hash-map k))
                [k v])))
       (remove nil?)
       (into {})
       not-empty))

(defn href
  "Returns a link for a (presumably modified) request map."
  [request]
  (let [request-str (pr-str (remove-empty-maps request))]
    (str "./?"
         (codec/url-encode (subs request-str 1 (dec (count request-str))))
         (some->> request :start (str "#")))))

(defn render-preview
  "Renders a photo preview."
  [request {:keys [digest]}]
  [:a {:href (href (assoc request :start digest))}
   [:img.preview {:style (style :aspect-ratio 1 :height "auto" :object-fit "cover")
                  :src (str digest "/thumbnail")}]])

(def preview-sizes
  "Style snippet to adaptively preview sizes through media queries."
  (->> (range 13)
       (map #(str "@media (min-width: " % "0em) { img.preview { width: calc(100vw / " (max % 3) ") } }"))
       (str/join "\n")))

(defn render-photo
  "Renders the currently focused photo spanning an entire viewport."
  [{:keys [digest]}]
  [:a {:href digest :style (style :display "flex")}
   [:img {:id digest
          :style (style :flex-grow 1 :max-width "100vw" :max-height "100vh" :object-fit "contain")
          :src (str digest "/preview")}]])

(defn format-float
  "Sensibly formats decimal numbers with at most 5 fraction digits
   which is not excessively long, yet enough to be usable with GPS coordinates in photos."
  [x]
  (.format (DecimalFormat. (apply str "#." (repeat 5 "#")))
           x))

(defn format-value
  "Formats a value as a string."
  [v]
  (or ({true "✓" nil "✗"} v)
      (cond (float? v) (format-float v)
            (vector? v) (->> v (map format-float) (str/join " "))
            :else v)))

(defn render-kv-text
  "Renders a label key-value pair."
  [k v]
  [:div {:style (style :display "flex" :flex-direction "column"
                       :background-color "#eee" :color "black"
                       :padding "1.25em 0.25em")}
   [:span k]
   [:span (format-value v)]])

(defn render-button
  "Renders a button."
  [request color text]
  [:a {:style (style :background-color color
                     :font-size "140%"
                     :color "inherit" :text-decoration "none" :font-weight "bold"
                     :padding "1.25em 0.5em")
       :href (href request)} text])

(defn render-filter-card
  "Renders the provided elements inside a filter card."
  [& elements]
  (apply vector
         :div
         {:style (style :border-radius "0.1em"
                        :overflow "hidden" :height "2.5em"
                        :display "flex" :flex-shrink 0 :align-items "center")}
         elements))

(defn render-controls
  "Renders the controls below a focused image."
  [photo {:keys [filters] :as request}]
  (let [remove-filter #(render-button (update request :filters dissoc %) "#f55" "X")
        sorted-by #(render-button (assoc request :sort-label %) "#ccf" "↕")
        limit-button (fn [k v bound label]
                       (render-button
                        (update-in request
                                   [:filters k]
                                   #(if (contains? (when (map? %) %) bound)
                                      (dissoc (when (map? %) %) bound)
                                      (merge (when (map? %) %)
                                             {bound v})))
                        (if (-> filters (get k) ((every-pred map? #(contains? % bound)))) "#f85" "#eee")
                        label))
        presence-button (fn [k v] (render-button (assoc-in request
                                                           [:filters k]
                                                           v)
                                                 ({nil "#a99" true "#bfa"} v)
                                                 (format-value v)))
        existing (map (fn [[k v]] (render-filter-card (remove-filter k)
                                                      (render-kv-text k v)))
                      (apply dissoc filters (keys photo)))
        from-photo (map (fn [[k v]]
                          (render-filter-card
                           (when-not (or (boolean? v) (vector? v)) (limit-button k v :min "↦"))
                           (render-kv-text k v)
                           (when-not (or (boolean? v) (vector? v)) (limit-button k v :max "↤"))
                           (presence-button k nil)
                           (presence-button k true)
                           (when-not (= (:sort-label request) k) (sorted-by k))
                           (when (contains? filters k) (remove-filter k))))
                        (sort-by (comp name key)
                                 (dissoc photo :files :version)))
        filter-boxes (concat existing
                             from-photo)]
    (apply vector :div {:style (style :display "flex" :flex-wrap "wrap" :overflow "auto" :max-height "50vh"
                                      :margin "0.5em 0" :gap "0.5em")}
           filter-boxes)))

(defn render-focused
  "Renders the focused photo and the correcsponding controls."
  [photo request]
  [:div
   (render-photo photo)
   (render-controls photo request)])

(defn render-previews
  "Renders the previous/next photos section above/below the focused photo."
  [request backwards photos]
  (->> photos
       (map (partial render-preview request))
       (apply vector :div {:style (style :display "flex" :line-height 0
                                         :flex-direction (str "row" (when backwards "-reverse"))
                                         :flex-wrap (str "wrap" (when backwards "-reverse")))})))

(defn page
  "Renders the HTML5 page to be sent to the client."
  [{:keys [state focused before after request]}]
  {:body
   (str "<!DOCTYPE html>" \newline
        (html [:html
               [:title (str "hxphotos" (when state (str " [" state "]")))]
               [:style preview-sizes]
               [:meta {:charset "utf-8"}]
               [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
               (when focused [:link {:rel "preload" :as "image" :href (str (:digest focused) "/preview")}])
               [:body {:style (style :background-color "#222"
                                     :font-family "sans-serif"
                                     :margin 0 :width "100vw")}
                (render-previews request true before)
                (some-> focused (render-focused request))
                (some->> after (render-previews request false))]]))})
