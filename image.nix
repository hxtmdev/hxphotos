{
  pkgs ? import <nixpkgs> {},
  ml ? true,
}: let
  clojure_headless = pkgs.clojure.override {jdk = pkgs.openjdk_headless;};
  overrides = {clojure = clojure_headless;};
  image_pkgs = pkgs // overrides;
  common = import nix/common.nix image_pkgs {ml = ml;};
  hxphotos = pkgs.callPackage nix/hxphotos.nix image_pkgs {ml = ml;};
in
  pkgs.dockerTools.streamLayeredImage {
    name = "registry.gitlab.com/hxtmdev/hxphotos";
    contents = pkgs.buildEnv {
      name = "image-root";
      paths = with pkgs;
        [
          hxphotos
          bashInteractive
          dockerTools.caCertificates
          dockerTools.usrBinEnv
        ]
        ++ common;
    };
    extraCommands = "mkdir -m 1777 tmp";
    config = {
      Cmd = ["/bin/hxphotos"];
      WorkingDir = "/data";
      Volumes = {"/cache" = {};};
      ExposedPorts = {"8080/tcp" = {};};
      Env = [
        "CACHEFILE=/cache/hxphotos.cache"
        "HOST=0.0.0.0"
      ];
    };
  }
