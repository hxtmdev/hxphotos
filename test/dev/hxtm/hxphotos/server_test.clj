(ns dev.hxtm.hxphotos.server-test
  (:require [clojure.test :refer :all]
            [dev.hxtm.hxphotos.config :as config]
            [dev.hxtm.hxphotos.server :refer :all])
  (:import [java.io File]))

(def ^:dynamic *test-data*
  "./test/")

(def tmp-cache (str (doto (File/createTempFile "hxphotos-" ".cache")
                      .deleteOnExit)))
(defn test-config [k fallback]
  (get (merge (some->> (System/getenv "SCRIPTS")
                       (hash-map :scripts))
              {:data *test-data*
               :cachefile tmp-cache})
       k fallback))

(def red "64ZcO5OnBoQc")
(def green "neXN2KLuzT-X")
(def blue "jAFYgRpX2vk0")

(deftest photo-analysis
  (with-redefs [config/config test-config
                state (atom {})]
    (testing "example scripts"
      (rescan-files)
      (testing "average-hue"
        (is (= (get-in @state [:photos red "average hue"]) [127 -127 -127]))
        (is (= (get-in @state [:photos green "average hue"]) [-128 127 -127]))
        (is (= (get-in @state [:photos blue "average hue"]) [-127 -127 127])))
      (testing "camera"
        (is (= (get-in @state [:photos red "camera"]) "make123 model456"))
        (is (not (contains? (get-in @state [:photos blue]) "camera"))))
      (testing "copies"
        (is (= (get-in @state [:photos red "copies"]) 1))
        (is (= (get-in @state [:photos blue "copies"]) 2)))
      (testing "date"
        (is (not (contains? (get-in @state [:photos red]) "date")))
        (is (= (get-in @state [:photos green "date"]) "2022-01-01 00:00:00"))
        (is (= (get-in @state [:photos blue "date"]) "2023-01-01 00:00:00")))
      (testing "directory"
        (is (every? #(= "files" %)
                    (->> @state
                         :photos
                         vals
                         (map #(get % "directory"))))))
      (testing "position"
        (is (= (get-in @state [:photos red "position"]) [89.5, 2.25]))
        (is (not (contains? (get-in @state [:photos blue]) "position"))))
      (testing "mobilenet (if installed)"
        (is (or (not (.exists (File. (config/config :scripts nil) "mobilenet.py")))
                (<= 10
                    (get-in @state [:photos blue "% jellyfish"])
                    13)))))
    (testing "rescanning"
      (testing "removes files that are now outside of DATA"
        (binding [*test-data* "./src/"]
          (rescan-files)
          (is (empty? (:photos @state))))))))
