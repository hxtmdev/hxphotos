pkgs @ {
  clojure,
  fetchurl,
  fetchgit,
  lib,
  nix-gitignore,
  openjdk_headless,
  fetchMavenArtifact,
  makeWrapper,
  stdenv,
  ...
}: opts @ {ml ? true}: let
  cp =
    (import ./deps.nix {inherit fetchMavenArtifact fetchgit lib;})
    .makeClasspaths {};
  cpTest =
    (import ./deps-test.nix {inherit fetchMavenArtifact fetchgit lib;})
    .makeClasspaths {};
  common = import ./common.nix pkgs opts;
in
  stdenv.mkDerivation rec {
    pname = "hxphotos";
    version = "1.0.0";
    src = nix-gitignore.gitignoreSource "/*.nix" ../.;
    buildInputs = [makeWrapper clojure];
    imagenetClasses = fetchurl {
      url = "https://storage.googleapis.com/download.tensorflow.org/data/imagenet_class_index.json";
      sha256 = "1g376w0j3w5sgj26kkq8xb9jljnxwyrikqa39fgd60gnl5kakrx1";
    };
    mobilenetWeigths = fetchurl {
      url = "https://storage.googleapis.com/tensorflow/keras-applications/mobilenet_v3/weights_mobilenet_v3_large_224_1.0_float.h5";
      sha256 = "0g6kzb8g7c0yf93wmlfbm6d7x0l4hcdy4m06wyd1qnjq0yxigmcv";
    };

    buildPhase = ''
      mkdir -p classes
      HOME=$PWD clojure -Scp "${cp}:src" -M -e "(compile 'dev.hxtm.hxphotos.server)"
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp -r classes $out/

      cp -r hxphotos_scripts/ $out/
      if ${
        if ml
        then "true"
        else "false"
      }
      then
        # pre-load mobilenet downloads
        mkdir -p $out/hxphotos_scripts/.keras/models
        for f in ${imagenetClasses} ${mobilenetWeigths};
        do
          cp -v --reflink=auto $f $out/hxphotos_scripts/.keras/models/$(stripHash $f)
        done
      else
        grep keras --files-with-matches $out/hxphotos_scripts/* | xargs rm
      fi

      makeWrapper ${openjdk_headless}/bin/java $out/bin/hxphotos \
        --prefix PATH : ${lib.makeBinPath common} \
        --run "echo Starting ${pname}" \
        --add-flags -Xmx1g \
        --add-flags -cp --add-flags "$out/classes:${cp}" \
        --add-flags dev.hxtm.hxphotos.server \
        --set-default SCRIPTS "$out/hxphotos_scripts"
    '';

    doInstallCheck = true;
    installCheckInputs = with pkgs; [clojure git] ++ common;
    installCheckPhase = ''
      HOME=$PWD SCRIPTS="$out/hxphotos_scripts" clojure -J-Djava.io.tmpdir="$PWD" -Scp "$out/classes:${cpTest}:test" -M -m cognitect.test-runner
    '';
  }
