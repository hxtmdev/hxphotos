{
  imagemagick,
  openjdk_headless,
  babashka,
  python3,
  coreutils-full,
  gnugrep,
  curl,
  wget,
  findutils,
  nodejs,
  exiftool,
  bash,
  ...
}: {ml ? true}:
[openjdk_headless exiftool imagemagick babashka bash coreutils-full gnugrep curl wget findutils]
++ (
  if ml
  then [(python3.withPackages (p: with p; [keras tensorflow]))]
  else []
)
