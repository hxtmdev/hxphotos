#!/usr/bin/env python3

# cache .keras downloads next to script file
import os
os.environ["HOME"] = os.path.dirname(__file__)

from sys import stdin
import sys
import json
import numpy as np
from keras.utils import load_img, img_to_array
from keras.applications.mobilenet_v3 import MobileNetV3Large, decode_predictions

# prevent keras download logging messing up stdout
realstdout = sys.stdout
sys.stdout = sys.stderr

model = MobileNetV3Large()

for line in stdin:
    files = json.loads(line)
    image = load_img(files[0], target_size=(224, 224), keep_aspect_ratio=True)
    predictions = model.predict(np.array([img_to_array(image)]), verbose=0)
    results = {"% " + description: round(probability * 100, 2)
               for _, description, probability
               in decode_predictions(predictions, top=7)[0]
               if probability >= 0.03}

    print(json.dumps(results), flush=True, file=realstdout)
