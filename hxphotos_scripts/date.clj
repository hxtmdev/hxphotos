#!/usr/bin/env bb

(some->> *command-line-args*
         (map #(.getName (io/file %)))
         (mapcat #(re-seq #"((?:19|20)\d\d)[^\d]?(\d\d)[^\d]?(\d\d)[^\d]?(\d\d)[^\d]?(\d\d)[^\d]?(\d\d)?" %))
         (filter #(> (count %) 5))
         (sort-by count)
         (map (fn [[_ y mo d h m s]]
                (str y "-" mo "-" d " " h ":" m ":" (or s "00"))))
         last
         (hash-map :date)
         json/encode
         println)
