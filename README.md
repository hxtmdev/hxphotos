# hxphotos

Lets you navigate your pile of photos along labels you extract with simple scripts.

## Screenshot

![Screenshot](doc/screenshot.webp)

- top to bottom: photos sorted before, focused photo, controls, photos sorted after
- currently sorted by date (missing sort button)
- an upper bound is set on the `% fire_screen` label

## Quickstart

If you don't care about details yet and just want to "make it go"
run this command from a directory with a couple of JPG photos you'd like to try this on.

```bash
docker run -p 127.0.0.1:8080:8080 -v $PWD:/data:ro -v hxphotos-cache:/cache registry.gitlab.com/hxtmdev/hxphotos
```

open http://localhost:8080/

## What's this?

This project is a prototype "exploring the question of how feasible it is to navigate one's unsorted photos along labels extracted by simple scripts".
In other words, the author did not feel like sorting photos, so he threw together something to obviate the need to do so.

**This is a prototype. Developers and enthusiasts might find it useful but it is not fit for use (installation at least) by non-technical users (yet?).**

### Goals

- **Web interface**: A web interface is an easy way for people in the same household / VPN to browse a shared photo collection.
- **Extensibility**: Photo labels such as date/time, image classification, and position should not be hardcoded but instead get extracted using custom scripts.
- **Easy scripting**: The amount of work necessary to implement a new label script with whatever tool fits best should be minimal.
- **Mobile compatible**: The interface should be usable on big screens and small screens, as well as with a mouse and a touchscreen.
- **Usable on wireless networks**: Photos should be compressed and scaled to save enough bandwidth to be usable on average wireless networks.
- **Simplicity**: While a certain complexity is unavoidable if we want to reach the other goals, it should be kept to the minimum necessary to solve the problem.

### Non-Goals

- **Performance**: Lots of performance optimizations have been sacrificed for simplicity and easy of scripting. That being said, once your local browser cache or caches in your reverse proxy setup are warmed up, it should be fairly usable with a couple of thousand photos.
- **Lots of features**: Prefer simple and useful approximations over correct but complex solutions. It's a read-only photo navigator and not a banking application after all.
- **Lots of included label scripts**: The included scripts are meant as a simple example and starting point rather than a comprehensive collection.

### How does it work?

1. Your photo directory is scanned for files ending in `.jpg`.
1. All those files are hashed and exact copies are grouped together to eliminate duplicates.
1. Your scripts are called to analyze the photos.
1. You can browse the photos using a web browser, filter by presence of a label, ranges, and "distance" to the currently focused photo, as well as sort along a label.

## Details

### Slim build

There is a `slim` / `x.z.y-slim` image tag that does not include Python with TensorFlow and Keras.
Use that one if you don't need those packages or the corresponding example script.

### Customize scripts

If you want to provide your own scripts, specify the directory in the environment as `SCRIPTS`.

If you want to bake your scripts right into the container image, start with the [Dockerfile template in doc/](doc/Dockerfile).
Make sure your scripts are executable (`chmod +x myscript.sh`)!

The provided images ship with the packages listed in [common.nix](./nix/common.nix).
Note that the slim tags omit the `ml` packages.

### Minimal permissions for the container

Assuming the user you run the container as has

- write permission for the cache directory
- read permission for the data directory

running as non-root should not be a problem.
Try `` -u `id -u`:`id -g` `` as a starting point.

If your scripts don't have to download anything or otherwise require internet access, the software does not need outgoing network access. The image ships with all dependencies for the included scripts.

### Performance considerations

The server process itself should be content with 1 GB of RAM. Additionally, the scripts need resources.

Photo analysis can take some time if you have many files. Simple scripts tend to incur some startup time for each photo, more optimized ones still only run with one instance at a time (different scripts run in parallel) to keep things simple - especially regarding some global resource / state your script might rely on.

Filtering and sorting seems to handle a couple of thousand unique photos reasonably well. If you have experience using it with more photos, please update this paragraph through a merge request.

### Writing scripts

Have a look at the [included example scripts](./hxphotos_scripts/).

Scripts are called with all the copies of a photo as arguments and in the simplest case just print lines with `key=value` pairs which are whitespace-trimmed and only used if both sides are not empty. All values are treated as strings.

Alternatively, you can print a JSON object as a line with the allowed value types being:

- `true` for simple flags
- a string for textual labels
- a number that will be sorted as such
- an array of numbers, this is meant for multi-dimensional labels such as positions and certain color use cases. "Sorting" in this case means sorting by Euclidian distance to the currently focused photo, with photos lacking that label being sorted "before" said focused photo. This enables navigating to photos that were taken nearby, have a similar color and more.

**It is your responsibility to ensure that all values for a label have the same type!**

If your script has a significant startup time, you can read a list of all the files for a photo encoded as JSON from stdin and print JSON objects on stdout, each one line per photo.

You can mix and match the input/output mechanisms. Scripts are always called with both input forms and output is streamed as JSON if the first character printed is `{`.

### Script failure

If your script crashes with a non-zero exit code (for `key=value` mode) or stops printing JSON objects (even mid-object) it is restarted with the remaining photos and if it fails again, there will be a delay of 5 seconds before the next retry. After that, the delay is multiplied by 1.5 each time until it yields a result again.

This enables recovering from temporary failures and (if you're so inclined) monkey-patching at runtime.

### Hiding photos

For this particular case, there's a "magic" label `hide`. Set it (preferably to `true`) and the photo will not be shown thanks to a hardcoded additional filter requiring it to be absent.

### Browser integration

The software does not do image caching by itself but instead relies on the browser or reverse proxies. Likewise, there is no client-side routing/history and, in fact, no JavaScript at all.

While this has some implications for performance, it keeps things simple and the standard browser features such as bookmarks / "open in new tab" / "share link" naturally just work without additional complexity.

### Authentication

The recommended solution to restrict access is to use a reverse proxy.

If you just require (very) Basic authentication set the environment variable `AUTH=user:password`.
When using Docker that's `-e AUTH=user:password`.

## Contributing

### Bugs

Browse and report bugs here: [gitlab.com/hxtmdev/hxphotos/-/issues](https://gitlab.com/hxtmdev/hxphotos/-/issues)

### Small changes

If you have a small improvement or bugfix that does not add much complexity or otherwise interferes with the project goals just open a merge request here: [gitlab.com/hxtmdev/hxphotos/-/merge_requests](https://gitlab.com/hxtmdev/hxphotos/-/merge_requests)

### Big changes

If you intend to make bigger changes or spend lots of time on something _and want it integrated into this repository_, please open an issue first to discuss it.
Of course, you have the option to maintain your own fork in line with the [license](./LICENSE).

### Commit requirements

All commits require a [DCO](https://developercertificate.org/) sign-off, which can be generated automatically by [calling `git commit` with `--signoff`](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---signoff).

Please include a body in every commit message with any details you think can help others understand what you did and why.

### Local development

For local development, use the Clojure tooling of your choice for interactive development.

When you're done, you can [install Nix](https://nixos.org/download.html), then run

```bash
nix-build && ./result/bin/hxphotos
```

to build and run the project.

To build and load a container image, run

```bash
$(nix-build image.nix) | docker load
```

Add `--arg ml false` after `nix-build` to build the `slim` variant.
